# Waveguide equivalent circuit

Theses notebooks describe a problem with representation of the waveguide as an equivalent circuit.

In file [matrix.ipynb](./matrix.ipynb) representation of the waveguide as an equivalent circuit is derived.

In file [type2.ipynb](./type2.ipynb) circuit equation is transformed to user distributed elements instead of lumped elements ones.

File [comparison.ipynb](./comparison.ipynb) contains comparison between equivalent element method and FEM
